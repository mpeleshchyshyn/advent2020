import java.util.*;

public class Day24 {

    private static final String DISTANCES = "Dubai, Denver, 1737\n" +
            "Dubai, Singapore, 678\n" +
            "Singapore, Brisbane, 386\n" +
            "Singapore, Perth, 348\n" +
            "Perth, Brisbane, 50\n" +
            "Perth, Auckland, 357\n" +
            "Brisbane, Auckland, 307\n" +
            "Brisbane, Denver, 1704\n" +
            "Auckland, Houston, 887\n" +
            "Auckland, Buenos Aires, 1015\n" +
            "Houston, Denver, 405\n" +
            "Houston, New York, 721\n" +
            "Houston, Buenos Aires, 225\n" +
            "Buenos Aires, New York, 702\n" +
            "Buenos Aires, Bangalore, 968\n" +
            "New York, Denver, 588\n" +
            "New York, Barcelona, 543\n" +
            "New York, Bangalore, 604\n" +
            "Bangalore, Barcelona, 923\n" +
            "Denver, Calgary, 238\n" +
            "Calgary, Aberdeen, 482\n" +
            "Calgary, Barcelona, 396\n" +
            "Calgary, London, 613\n" +
            "Aberdeen, London, 190\n" +
            "London, Paris, 81\n" +
            "Paris, Barcelona, 123";

    private Graph graph = new Graph();
    int minPath = Integer.MAX_VALUE;

    private Day24() {
    }


    public static void main(String[] args) {
        Day24 test = new Day24();
        test.test(DISTANCES);
    }


    private void test(String distances) {
        String[] lines = distances.split("\n");
        for (String line : lines) {
            String[] segments = line.split(",");
            graph.addBidirectionalConnection(segments[0].trim(), segments[1].trim(), Integer.valueOf(segments[2].trim()));
        }

        List<String> nodes = graph.getNodeNames();
        //all possible pairs
//        for (int i = 0; i < nodes.size() - 1; i++) {
//            for (int j = i + 1; j < nodes.size(); j++) {
//                String node1 = nodes.get(i);
//                String node2 = nodes.get(j);
//                System.out.println("Pair: " + node1 + ", " + node2);
//                allPaths(node1, node2);
//            }
//        }
        allPaths("Perth", "London");
        System.out.println("-------------------------------------------------------");
        System.out.println("MIN = " + minPath);
    }

    private void allPaths(String start, String end) {
        Set<String> visited = new HashSet<>();
        List<String> path = new ArrayList<>();
        path.add(start);
        findPath(start, end, visited, path);
    }

    private void findPath(String current, String end, Set<String> visited, List<String> path) {
        if (current.equals(end)) {
            if(path.size() == graph.size()) {
                System.out.println("--------------------------------------");
                System.out.println("ALL NODES");
            }
            System.out.println(path);
            checkMin(path);
            return;
        }

        visited.add(current);

        // Recur for all the vertices
        // adjacent to current vertex
        Map<String, Integer> connections = graph.getNode(current).getConnections();
        connections.keySet().forEach(other -> {
            if (!visited.contains(other)) {
                path.add(other);
                findPath(other, end, visited, path);
                path.remove(other);
            }
        });
        visited.remove(current);
    }

    private void checkMin(List<String> path) {
        int totalDistance = 0;
        for (int i = 0; i < path.size() - 1; i++) {
            totalDistance = totalDistance + graph.getDistance(path.get(i), path.get(i + 1));
        }
        System.out.println("Dist = " + totalDistance);
        if (totalDistance < minPath) {
            minPath = totalDistance;
        }
    }
}
