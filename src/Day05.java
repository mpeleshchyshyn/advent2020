public class Day05 {

    private Day05() {
    }

    public static void main(String[] args) {
        Day05 t = new Day05();
        t.testNumbers();
    }

    private void testNumbers() {
        int count = 0;
        for(int i = 249905; i<= 767253; i++) {
            if(testNumber(i)) {
                System.out.println(i);
                count++;
            }
        }

        System.out.println("Count = " + count);
    }

    private boolean testNumber(int number) {
        String str = Integer.toString(number);
        boolean hasDup = false;
        for(int i=0; i<str.length() -1;i++) {
            char currChar = str.charAt(i);
            char nextChar = str.charAt(i+1);
            if(currChar == nextChar) {
                hasDup = true;
            }
            if(nextChar<currChar) {
                return false;
            }
        }

        return hasDup;
    }
}
