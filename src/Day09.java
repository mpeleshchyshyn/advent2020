import java.io.BufferedReader;
import java.io.InputStreamReader;

/*
Records obtained using GraphQL query:
{
  naughtyNiceList {
    people {
      name
      deeds {
        naughty {
          __typename
          ... on DeedList {
            list {
              records {
                value
              }
            }
          }
        }
      }
    }
  }
}

Proper answer:
{
  naughtyNiceList {
    people {
      name
      deeds {
        naughty(sum:true) {
          __typename
          ... on AggResult {
            value
          }
        }
      }
    }
  }
}
 */

public class Day09 {
    private Day09() {
    }

    public static void main(String[] args) throws Exception {
        Day09 test = new Day09();
        test.test();
    }

    private void test() throws Exception {
        String maxPerson = "NOT FOUND";
        int max = 0;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("Day 9.json")))) {
            String line;
            String currUser = maxPerson;
            int currSum = 0;
            while ((line = reader.readLine()) != null) {
                if (line.contains("\"name\"")) {
                    System.out.println(currSum);
                    System.out.println("-----------------------------------------");
                    System.out.println(line);
                    if (currSum > max) {
                        maxPerson = currUser;
                        max = currSum;
                    }
                    //reset user and sum
                    currUser = line;
                    currSum = 0;
                } else if (line.contains("value")) {
                    String[] segments = line.split(":");
                    currSum = currSum + Integer.valueOf(segments[1].trim());
                }
            }
            System.out.println(currSum);
            System.out.println("-----------------------------------------");
            if (currSum > max) {
                maxPerson = currUser;
                max = currSum;
            }
            System.out.println(" --------------      MAX              ----------------------------------");
            System.out.println(maxPerson);
            System.out.println(max);
        }
    }
}
