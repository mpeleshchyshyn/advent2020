import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Day19 {
    private static final int[] SAMPLE_DATA_1 = new int[]{4, 2, 3, 0, 3, 1, 2};
    private static final int[] SAMPLE_DATA_2 = new int[]{2, 1, 1, 2, 4, 3, 3, 0, 1, 1, 2};
    private static final int[] SAMPLE_DATA_3 = new int[]{3, 0, 2, 1, 2};

    private static final Map<Character, int[]> DATA = Map.of(
            'A', new int[]{4, 4, 6, 4, 3, 2, 0, 8, 2, 4, 0, 8, 9, 4, 5, 6, 6, 3, 3, 4, 9, 9, 8, 3, 7, 6, 5, 6, 3, 7, 7, 6, 9, 9, 9, 8, 4, 2, 5, 2, 7, 7, 8, 5, 6, 6, 2, 5, 6, 3, 9, 9, 5, 3, 9, 5, 6, 2, 6, 6, 6, 3, 5, 4, 6, 3, 9, 0, 4, 7, 1, 4, 7, 1, 6, 1, 1, 2, 7, 0, 3, 0, 9, 9, 8, 7, 0, 6, 9, 4, 1, 2, 5, 8, 5, 4, 2, 4, 8, 2},
            'B', new int[]{9, 2, 2, 6, 8, 9, 1, 9, 1, 1, 8, 6, 7, 1, 1, 7, 5, 8, 5, 7, 9, 1, 5, 9, 0, 2, 4, 7, 7, 8, 3, 2, 4, 3, 3, 2, 3, 2, 7, 8, 5, 6, 7, 4, 5, 5, 9, 6, 6, 1, 6, 3, 2, 3, 4, 4, 6, 3, 1, 3, 1, 3, 2, 0, 3, 3, 2, 2, 1, 9, 3, 2, 5, 6, 6, 3, 4, 4, 4, 7, 1, 1, 1, 5, 2, 4, 4, 1, 8, 5, 4, 3, 3, 3, 1, 2, 4, 3, 7, 0},
            'C', new int[]{1, 3, 5, 5, 7, 2, 2, 6, 8, 4, 4, 9, 6, 5, 9, 3, 4, 2, 5, 7, 6, 9, 6, 3, 1, 1, 5, 5, 5, 1, 6, 1, 6, 1, 3, 5, 1, 4, 2, 5, 6, 9, 3, 2, 3, 4, 5, 8, 4, 9, 4, 1, 1, 9, 6, 1, 7, 1, 4, 6, 6, 6, 1, 7, 7, 2, 3, 5, 1, 4, 2, 0, 9, 0, 4, 6, 5, 3, 0, 7, 6, 6, 0, 6, 7, 6, 0, 7, 4, 1, 8, 0, 0, 6, 8, 9, 8, 6, 5, 1},
            'D', new int[]{4, 9, 2, 7, 3, 5, 6, 5, 7, 7, 9, 9, 5, 1, 4, 5, 6, 9, 5, 1, 9, 2, 9, 5, 6, 8, 5, 2, 4, 8, 5, 6, 7, 6, 8, 6, 4, 3, 1, 8, 0, 6, 2, 6, 8, 4, 0, 0, 4, 9, 0, 1, 2, 2, 1, 2, 9, 7, 1, 5, 9, 4, 0, 9, 2, 2, 2, 9, 3, 7, 1, 3, 7, 6, 1, 3, 9, 2, 6, 2, 8, 5, 7, 4, 7, 4, 4, 5, 5, 5, 7, 6, 2, 6, 9, 5, 5, 3, 4, 6},
            'E', new int[]{8, 3, 9, 2, 7, 5, 8, 4, 8, 6, 8, 4, 6, 8, 5, 9, 3, 5, 4, 8, 4, 8, 1, 2, 1, 5, 8, 4, 4, 5, 7, 7, 6, 7, 2, 2, 9, 9, 8, 2, 9, 6, 1, 9, 6, 4, 5, 5, 5, 3, 2, 1, 4, 2, 9, 7, 1, 7, 1, 1, 6, 4, 9, 2, 9, 3, 8, 8, 5, 5, 9, 4, 7, 4, 9, 7, 6, 1, 6, 6, 9, 3, 8, 1, 2, 5, 6, 7, 7, 2, 6, 1, 1, 2, 4, 3, 6, 8, 6, 0},
            'F', new int[]{3, 0, 6, 3, 2, 1, 4, 5, 1, 3, 2, 9, 6, 0, 6, 2, 5, 7, 6, 4, 7, 2, 4, 4, 3, 7, 8, 4, 7, 5, 1, 8, 7, 1, 7, 7, 8, 2, 6, 8, 3, 3, 5, 6, 6, 4, 9, 5, 2, 1, 7, 1, 3, 2, 9, 0, 5, 1, 0, 4, 2, 0, 8, 5, 2, 3, 1, 5, 2, 9, 4, 2, 0, 7, 1, 4, 7, 7, 7, 8, 8, 2, 1, 5, 8, 0, 5, 5, 1, 7, 1, 6, 2, 2, 5, 2, 5, 9, 4, 2},
            'G', new int[]{9, 6, 6, 8, 9, 6, 6, 8, 5, 9, 3, 7, 8, 6, 9, 1, 3, 7, 1, 4, 2, 3, 7, 7, 2, 9, 4, 5, 2, 4, 6, 1, 8, 8, 1, 5, 1, 2, 8, 2, 8, 2, 2, 4, 2, 2, 0, 2, 3, 6, 7, 9, 1, 6, 0, 5, 9, 8, 6, 9, 2, 1, 5, 1, 2, 2, 9, 0, 4, 4, 0, 0, 9, 9, 6, 0, 0, 2, 4, 9, 9, 1, 2, 5, 6, 6, 9, 1, 7, 5, 5, 2, 6, 8, 0, 6, 6, 1, 1, 0},
            'H', new int[]{1, 1, 7, 7, 8, 7, 3, 5, 1, 8, 2, 6, 1, 9, 7, 4, 7, 6, 2, 7, 8, 7, 3, 7, 9, 4, 1, 1, 7, 5, 7, 6, 8, 9, 0, 6, 7, 0, 6, 9, 0, 9, 3, 7, 3, 1, 5, 3, 9, 4, 8, 9, 1, 6, 8, 4, 7, 0, 7, 3, 6, 2, 7, 5, 0, 5, 8, 1, 8, 0, 7, 6, 9, 2, 7, 8, 3, 6, 3, 8, 9, 7, 1, 3, 6, 8, 5, 1, 1, 5, 9, 7, 2, 2, 3, 7, 1, 8, 7, 7},
            'I', new int[]{5, 8, 6, 6, 9, 3, 2, 9, 3, 7, 5, 7, 3, 3, 5, 1, 1, 5, 6, 2, 4, 4, 4, 4, 4, 0, 2, 5, 4, 4, 7, 9, 7, 6, 3, 0, 7, 8, 8, 6, 9, 3, 7, 7, 4, 1, 7, 9, 1, 0, 2, 3, 1, 7, 0, 1, 3, 2, 1, 9, 0, 6, 5, 8, 0, 6, 0, 6, 0, 5, 3, 2, 7, 0, 2, 0, 6, 7, 9, 3, 2, 2, 4, 3, 2, 3, 7, 0, 5, 1, 6, 5, 7, 8, 5, 2, 8, 9, 7, 7},
            'J', new int[]{2, 2, 3, 9, 3, 5, 6, 9, 1, 3, 6, 8, 2, 2, 5, 6, 2, 4, 5, 5, 7, 2, 1, 3, 5, 1, 5, 6, 7, 8, 2, 7, 6, 7, 9, 4, 3, 6, 4, 4, 5, 5, 5, 5, 3, 4, 2, 1, 3, 1, 6, 8, 2, 1, 5, 9, 0, 9, 3, 2, 0, 0, 2, 3, 0, 9, 2, 5, 3, 0, 3, 6, 0, 0, 0, 2, 6, 6, 0, 0, 8, 3, 3, 6, 1, 4, 4, 4, 2, 9, 3, 3, 2, 2, 3, 5, 3, 2, 5, 4}
    );

    public static void main(String[] args) {
        Day19 test = new Day19();
//        int jumps = test.test(SAMPLE_DATA_3);
//        System.out.println("Jumps count = " + jumps);
        test.testMultiple(DATA);
    }

    private void testMultiple(Map<Character, int[]> data) {
        int min = Integer.MAX_VALUE;
        char minRouteName = 0;
        for (Map.Entry<Character,int[]> entry: data.entrySet()) {
            int jumps = test(entry.getValue());
            System.out.println(entry.getKey() + ": " + jumps);
            if(jumps < min && jumps > -1) {
                min = jumps;
                minRouteName = entry.getKey();
            }

        }
        System.out.println("MIN route: " + minRouteName);
    }

    private int test(int[] data) {
        List<Integer> visited = visitIndex(data, 0, Collections.emptyList());
//        System.out.println("VISITED: " + visited);
        return visited != null ? visited.size() - 1 : -1;
    }

    private List<Integer> visitIndex(int[] data, int pos, List<Integer> visited) {
        if (visited.contains(pos)) {
//            System.err.println("LOOP!");
            return null;
        }
        try {
            int steps = data[pos];
            List<Integer> visitedCopy = new ArrayList<>(visited);
            visitedCopy.add(pos);
            if (steps == 0) {
                //END
                return visitedCopy;
            }
            List<Integer> visitedOption1 = visitIndex(data, pos + steps, visitedCopy);
            List<Integer> visitedOption2 = visitIndex(data, pos - steps, visitedCopy);
            if (visitedOption1 != null && visitedOption2 != null) {
                return visitedOption1.size() < visitedOption2.size() ? visitedOption1 : visitedOption2;
            } else if (visitedOption1 != null) {
                return visitedOption1;
            } else if (visitedOption2 != null) {
                return visitedOption2;
            }
            return null;
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }
}
