import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

class Graph {
    private Map<String, Node> nodes = new TreeMap<>();

    List<String> getNodeNames() {
        return new ArrayList(nodes.keySet());
    }

    int getDistance(String node1, String node2) {
        return nodes.get(node1).getConnections().get(node2);
    }

    int size() {
        return nodes.size();
    }

    Node getNode(String name) {
        return nodes.get(name);
    }

    void addBidirectionalConnection(String node1Name, String node2Name, int distance) {
        addConnection(node1Name, node2Name, distance);
        addConnection(node2Name, node1Name, distance);
    }

    private void addConnection(String node1Name, String node2Name, int distance) {
        Node node1 = nodes.computeIfAbsent(node1Name, nodeName -> new Node(nodeName));
        nodes.computeIfAbsent(node2Name, nodeName -> new Node(nodeName));
        node1.addConnection(node2Name, distance);
    }

    class Node {
        private final String name;
        private Map<String, Integer> connections = new TreeMap<>();

        Node(String name) {
            this.name = name;
        }

        String getName() {
            return name;
        }

        private void addConnection(String nodeName, int distance) {
            connections.put(nodeName, distance);
        }

        Map<String, Integer> getConnections() {
            return connections;
        }
    }
}
