import java.io.BufferedReader;
import java.io.StringReader;
import java.util.SortedMap;
import java.util.TreeMap;

public class Day11 {

//    private static String DATA = "LXXXIX,  fd 25\n" +
//            "XL,      fd 50\n" +
//            "XXXIV,   penup\n" +
//            "XIII,    rt 45\n" +
//            "XCIX,    label [HERE]\n" +
//            "VI,      clearscreen window hideturtle\n" +
//            "XLVIII,  rt 90\n" +
//            "XVIII,   label [RIGHT]\n" +
//            "IX,      setlabelheight 50";

    private static String DATA = "LXXIV,  lt 90\n" +
            "XVII,  fd :diameter / 2\n" +
            "CXVIII,  rt 180\n" +
            "XXIV,  rt 90\n" +
            "XXX,  rt 90\n" +
            "LXIII,  pd\n" +
            "XLVI,  pu\n" +
            "VIII,  fd :diameter / 4\n" +
            "CIII,  pu\n" +
            "XC,END\n" +
            "XXXIV,  lt 90\n" +
            "LXIX,  pu\n" +
            "II,  lt 90\n" +
            "XCIII,  setxy :offset 0\n" +
            "CIV,END\n" +
            "LXXVII,  pu\n" +
            "XXVII,  bk 35\n" +
            "LXI,TO three :offset\n" +
            "LXXIX,END\n" +
            "XXXVIII,TO one :offset\n" +
            "LXII,  setxy :offset 0\n" +
            "LXVII,  pd\n" +
            "LXXXVI,  fd 115\n" +
            "LXXXIII,  pd\n" +
            "IX,  pu\n" +
            "LIX,END\n" +
            "CXXXVIII,one -280\n" +
            "XII,  lt 90\n" +
            "LXXI,  curve 50\n" +
            "XLVII,END\n" +
            "XX,\n" +
            "XXXVII,\n" +
            "LVII,  rt 180\n" +
            "LIII,  fd 70\n" +
            "CXXIV,  rt 180\n" +
            "CXXXIX,two -200\n" +
            "XXXV,  pu\n" +
            "IV,  pu\n" +
            "CXXIII,  pd\n" +
            "CXI,  pu\n" +
            "XLIV,  curve 50\n" +
            "XCIV,  pd\n" +
            "CVI,TO six :offset\n" +
            "LII,  pd\n" +
            "CXV,  pu\n" +
            "CXXV,  fd 100\n" +
            "CXLV,ht\n" +
            "XCV,  fd 100\n" +
            "CXXVII,  fd 50\n" +
            "LXXXIX,  pu\n" +
            "XXI,TO zero :offset\n" +
            "LI,  rt 90\n" +
            "LXXVIII,  rt 90\n" +
            "LXX,  setxy :offset + 25 75\n" +
            "CXXIX,  pu\n" +
            "L,  setxy :offset 100\n" +
            "CXXI,TO seven :offset\n" +
            "CX,  fd 75\n" +
            "XI,  fd :diameter\n" +
            "XXXII,  rt 180\n" +
            "XVI,  lt 90\n" +
            "LV,  rt 90\n" +
            "CVIII,  rt 180\n" +
            "LXXXI,TO four :offset\n" +
            "XLI,  curve 50\n" +
            "LXXXIV,  fd 100\n" +
            "CXXXIII,st\n" +
            "XIII,  pd\n" +
            "XXXIX,  setxy :offset + 46 78\n" +
            "CII,  lt 90\n" +
            "XCVIII,  setxy :offset + 15 75\n" +
            "XVIII,  rt 90\n" +
            "CI,  curve 50\n" +
            "XCI,\n" +
            "XIX,END\n" +
            "LXV,  pu\n" +
            "XCVI,  pu\n" +
            "LXXX,\n" +
            "CXVII,  curve 50\n" +
            "XLIII,  setxy :offset + 54 22\n" +
            "CXX,\n" +
            "LXVI,  setxy :offset + 50 0\n" +
            "CVII,  setxy :offset 100\n" +
            "XIV,  fd :diameter / 4\n" +
            "LXVIII,  fd 75\n" +
            "CXII,  setxy :offset + 50 100\n" +
            "LIV,  bk 35\n" +
            "LXXXVII,  lt 150\n" +
            "LXXIII,  setxy :offset + 50 50\n" +
            "CV,\n" +
            "XCVII,  rt 90\n" +
            "CXVI,  setxy :offset + 25 25\n" +
            "XXXI,  fd 35\n" +
            "XLIX,TO two :offset\n" +
            "CXXXI,\n" +
            "CXXII,  setxy :offset 100\n" +
            "CXIV,  fd 75\n" +
            "XLII,  rt 180\n" +
            "CXIII,  pd\n" +
            "CIX,  pd\n" +
            "LX,\n" +
            "CXXXV,setwidth 3\n" +
            "X,  lt 90\n" +
            "CXXXII,window cs\n" +
            "LXXXV,  rt 150\n" +
            "XXII,  pu\n" +
            "XXVI,  fd 70\n" +
            "XLV,  lt 110\n" +
            "CXL,three -100\n" +
            "CXXXVII,zero -350\n" +
            "XCII,TO five :offset\n" +
            "XXIII,  setxy :offset 100\n" +
            "I,TO curve :diameter\n" +
            "LVIII,  pu\n" +
            "CXXXVI,\n" +
            "CXLI,four 0\n" +
            "LXXV,  pd\n" +
            "XXXVI,END\n" +
            "VII,  pd\n" +
            "LXXVI,  fd 50\n" +
            "XXXIII,  fd 70\n" +
            "LXXII,  pu\n" +
            "XXVIII,  rt 90\n" +
            "CXLIII,six 175\n" +
            "III,  arc 180 :diameter / 2\n" +
            "XLVIII,\n" +
            "LVI,  fd 100\n" +
            "CXLIV,seven 260\n" +
            "XXV,  pd\n" +
            "XL,  lt 70\n" +
            "XV,  pu\n" +
            "C,  setxy :offset + 15 25\n" +
            "XCIX,  curve 50\n" +
            "CXXVIII,  lt 90\n" +
            "V,  fd :diameter / 2\n" +
            "CXXXIV,setpencolor 1\n" +
            "XXIX,  fd 100\n" +
            "CXXX,END\n" +
            "LXIV,  fd 75\n" +
            "CXIX,END\n" +
            "CXLII,five 100\n" +
            "CXXVI,  lt 90\n" +
            "LXXXII,  setxy :offset 0\n" +
            "VI,  lt 90\n" +
            "LXXXVIII,  fd 100";

    private Day11() {
    }

    public static void main(String[] args) throws Exception {
        Day11 test = new Day11();
        test.test();
        //Run result via https://www.calormen.com/jslogo/
    }

    private void test() throws Exception {
        SortedMap<Integer, String> result = new TreeMap<>();
        try(BufferedReader reader = new BufferedReader(new StringReader(DATA))){
            String line;
            int lineNo = 1;
            while((line=reader.readLine())!=null) {
                String[] segments = line.split(",");
                try {
                    result.put(convertToDecimal(segments[0]), segments[1]);
                }
                catch(ArrayIndexOutOfBoundsException e) {
//                    System.err.println("Error in line # " + lineNo + ": " + line);
                    result.put(convertToDecimal(segments[0]), "");
                }
                lineNo++;
            }
        }
        result.values().forEach(v -> System.out.println(v));
    }

    private static int convertToDecimal(String romanNumeral) {
        int l = romanNumeral.length();
        int num = 0;
        int previousnum = 0;
        int decimalNum = 0;
        for (int i = l - 1; i >= 0; i--) {
            char x = romanNumeral.charAt(i);
            x = Character.toUpperCase(x);
            switch (x) {
                case 'I':
                    previousnum = num;
                    num = 1;
                    break;
                case 'V':
                    previousnum = num;
                    num = 5;
                    break;
                case 'X':
                    previousnum = num;
                    num = 10;
                    break;
                case 'L':
                    previousnum = num;
                    num = 50;
                    break;
                case 'C':
                    previousnum = num;
                    num = 100;
                    break;
                case 'D':
                    previousnum = num;
                    num = 500;
                    break;
                case 'M':
                    previousnum = num;
                    num = 1000;
                    break;
            }
            decimalNum = num < previousnum ? decimalNum - num : decimalNum + num;
        }

        return decimalNum;
    }
}
