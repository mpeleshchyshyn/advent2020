import java.util.*;

public class Day21 {
    private static final String SAMPLE_DISTANCES = "London to Aberdeen = 464\n" +
            "London to Amsterdam = 518\n" +
            "Aberdeen to Amsterdam = 141";

    private static final String DISTANCES = "Calgary to Houston = 66\n" +
            "Calgary to Brisbane = 28\n" +
            "Calgary to Buenos_Aires = 60\n" +
            "Calgary to Singapore = 34\n" +
            "Calgary to Bogota = 34\n" +
            "Calgary to Aberdeen = 3\n" +
            "Calgary to London = 108\n" +
            "Houston to Brisbane = 22\n" +
            "Houston to Buenos_Aires = 12\n" +
            "Houston to Singapore = 91\n" +
            "Houston to Bogota = 121\n" +
            "Houston to Aberdeen = 111\n" +
            "Houston to London = 71\n" +
            "Brisbane to Buenos_Aires = 39\n" +
            "Brisbane to Singapore = 113\n" +
            "Brisbane to Bogota = 130\n" +
            "Brisbane to Aberdeen = 35\n" +
            "Brisbane to London = 40\n" +
            "Buenos_Aires to Singapore = 63\n" +
            "Buenos_Aires to Bogota = 21\n" +
            "Buenos_Aires to Aberdeen = 57\n" +
            "Buenos_Aires to London = 83\n" +
            "Singapore to Bogota = 9\n" +
            "Singapore to Aberdeen = 50\n" +
            "Singapore to London = 60\n" +
            "Bogota to Aberdeen = 27\n" +
            "Bogota to London = 81\n" +
            "Aberdeen to London = 90";

    private Graph graph = new Graph();
    int minPath = Integer.MAX_VALUE;

    private Day21() {
    }


    public static void main(String[] args) {
        Day21 test = new Day21();
        test.test(DISTANCES);
    }


    private void test(String distances) {
        String[] lines = distances.split("\n");
        for (String line : lines) {
            String[] segments = line.split(" ");
            graph.addBidirectionalConnection(segments[0], segments[2], Integer.valueOf(segments[4]));
        }

        List<String> nodes = graph.getNodeNames();
        //all possible pairs
        for (int i = 0; i < nodes.size() - 1; i++) {
            for (int j = i + 1; j < nodes.size(); j++) {
                String node1 = nodes.get(i);
                String node2 = nodes.get(j);
                System.out.println("Pair: " + node1 + ", " + node2);
                allPaths(node1, node2);
            }
        }
//        allPaths("Aberdeen", "London");
        System.out.println("-------------------------------------------------------");
        System.out.println("MIN = " + minPath);
    }

    private void allPaths(String start, String end) {
        Set<String> visited = new HashSet<>();
        List<String> path = new ArrayList<>();
        path.add(start);
        findPath(start, end, visited, path);
    }

    private void findPath(String current, String end, Set<String> visited, List<String> path) {
        if (current.equals(end)) {
            if (path.size() == graph.size()) {
                System.out.println(path);
                checkMin(path);
            }
            return;
        }

        visited.add(current);

        // Recur for all the vertices
        // adjacent to current vertex
        Map<String, Integer> connections = graph.getNode(current).getConnections();
        connections.keySet().forEach(other -> {
            if (!visited.contains(other)) {
                path.add(other);
                findPath(other, end, visited, path);
                path.remove(other);
            }
        });
        visited.remove(current);
    }

    private void checkMin(List<String> path) {
        int totalDistance = 0;
        for (int i = 0; i < path.size() - 1; i++) {
            totalDistance = totalDistance + graph.getDistance(path.get(i), path.get(i + 1));
        }
        System.out.println("Dist = " + totalDistance);
        if (totalDistance < minPath) {
            minPath = totalDistance;
        }
    }
}
