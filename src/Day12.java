import java.awt.*;

public class Day12 {
//    private static final String DIRECTIONS = "R5, L2, R1, R3, L1, R3, R4, R1, L1";
    private static final String DIRECTIONS = "R5, L5, R7, R13, R19, R4, L14, R5, L4, L6, R3, L12, L3, R16, L3, L7, L3, R12, R1, L13, L12, R9, L4, L3, R5, L14, R5, L5, R4, L4, L6, R3, L12, L3, R16, L4, L3, L3, R7, R13, R19, L7, L3, R12, R1, L13, L12, R9, L4, L3";

    private Point point = new Point();
    private double direction = Math.PI / 2;

    private Day12() {
    }

    public static void main(String[] args) {
        Day12 test = new Day12();
        test.test(DIRECTIONS);
    }

    private void test(String insructions) {
        String[] segments = insructions.split(",");
        for(String segment: segments) {
            String trimmedSegment = segment.trim();
            char instruction = trimmedSegment.charAt(0);
            int distance = Integer.valueOf(trimmedSegment.substring(1));
            direction = instruction == 'R' ? direction - Math.PI / 2 : direction + Math.PI / 2;
//            System.out.println("COS = " + Math.round(Math.cos(direction)));
//            System.out.println("SIN = " + Math.round(Math.sin(direction)));
            point.x = point.x + (int) Math.round(Math.cos(direction) * distance);
            point.y = point.y + (int) Math.round(Math.sin(direction) * distance);
            System.out.println(point);
        }
        System.out.println("-----------------------");
        System.out.println("End point: " + point);
        double cost = 6.50 + 3.56 * (Math.abs(point.x) + Math.abs(point.y));
        System.out.println("Cost = " + cost);
    }
}
