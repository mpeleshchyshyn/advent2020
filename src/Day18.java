import java.util.*;
import java.util.stream.Collectors;

public class Day18 {
    private static final int BEFORE_NODE_POS = 5;
    private static final int AFTER_NODE_POS = 37;

    private static final String SAMPLE_DATA = "Jump C must be completed before jump A can start.\n" +
            "Jump C must be completed before jump F can start.\n" +
            "Jump A must be completed before jump B can start.\n" +
            "Jump A must be completed before jump D can start.\n" +
            "Jump B must be completed before jump E can start.\n" +
            "Jump D must be completed before jump E can start.\n" +
            "Jump F must be completed before jump E can start.";

    private static final String DATA = "Jump A must be completed before jump I can start.\n" +
            "Jump M must be completed before jump Q can start.\n" +
            "Jump B must be completed before jump S can start.\n" +
            "Jump G must be completed before jump N can start.\n" +
            "Jump Y must be completed before jump R can start.\n" +
            "Jump E must be completed before jump H can start.\n" +
            "Jump K must be completed before jump L can start.\n" +
            "Jump H must be completed before jump Z can start.\n" +
            "Jump C must be completed before jump P can start.\n" +
            "Jump W must be completed before jump U can start.\n" +
            "Jump V must be completed before jump L can start.\n" +
            "Jump O must be completed before jump N can start.\n" +
            "Jump U must be completed before jump I can start.\n" +
            "Jump D must be completed before jump P can start.\n" +
            "Jump Q must be completed before jump L can start.\n" +
            "Jump F must be completed before jump Z can start.\n" +
            "Jump L must be completed before jump N can start.\n" +
            "Jump P must be completed before jump S can start.\n" +
            "Jump I must be completed before jump S can start.\n" +
            "Jump S must be completed before jump R can start.\n" +
            "Jump T must be completed before jump N can start.\n" +
            "Jump N must be completed before jump X can start.\n" +
            "Jump Z must be completed before jump J can start.\n" +
            "Jump R must be completed before jump J can start.\n" +
            "Jump J must be completed before jump X can start.\n" +
            "Jump E must be completed before jump I can start.\n" +
            "Jump T must be completed before jump R can start.\n" +
            "Jump I must be completed before jump N can start.\n" +
            "Jump K must be completed before jump C can start.\n" +
            "Jump B must be completed before jump D can start.\n" +
            "Jump K must be completed before jump T can start.\n" +
            "Jump E must be completed before jump P can start.\n" +
            "Jump F must be completed before jump I can start.\n" +
            "Jump O must be completed before jump U can start.\n" +
            "Jump I must be completed before jump J can start.\n" +
            "Jump S must be completed before jump Z can start.\n" +
            "Jump L must be completed before jump J can start.\n" +
            "Jump F must be completed before jump T can start.\n" +
            "Jump F must be completed before jump P can start.\n" +
            "Jump I must be completed before jump T can start.\n" +
            "Jump G must be completed before jump S can start.\n" +
            "Jump V must be completed before jump U can start.\n" +
            "Jump F must be completed before jump R can start.\n" +
            "Jump L must be completed before jump R can start.\n" +
            "Jump Y must be completed before jump D can start.\n" +
            "Jump M must be completed before jump E can start.\n" +
            "Jump U must be completed before jump L can start.\n" +
            "Jump C must be completed before jump D can start.\n" +
            "Jump W must be completed before jump N can start.\n" +
            "Jump S must be completed before jump N can start.\n" +
            "Jump O must be completed before jump S can start.\n" +
            "Jump B must be completed before jump T can start.\n" +
            "Jump V must be completed before jump T can start.\n" +
            "Jump S must be completed before jump X can start.\n" +
            "Jump V must be completed before jump P can start.\n" +
            "Jump F must be completed before jump L can start.\n" +
            "Jump P must be completed before jump R can start.\n" +
            "Jump D must be completed before jump N can start.\n" +
            "Jump C must be completed before jump L can start.\n" +
            "Jump O must be completed before jump Q can start.\n" +
            "Jump N must be completed before jump Z can start.\n" +
            "Jump Y must be completed before jump L can start.\n" +
            "Jump B must be completed before jump K can start.\n" +
            "Jump P must be completed before jump Z can start.\n" +
            "Jump V must be completed before jump Z can start.\n" +
            "Jump U must be completed before jump J can start.\n" +
            "Jump Q must be completed before jump S can start.\n" +
            "Jump H must be completed before jump F can start.\n" +
            "Jump E must be completed before jump O can start.\n" +
            "Jump D must be completed before jump F can start.\n" +
            "Jump D must be completed before jump X can start.\n" +
            "Jump L must be completed before jump S can start.\n" +
            "Jump Z must be completed before jump R can start.\n" +
            "Jump K must be completed before jump X can start.\n" +
            "Jump M must be completed before jump V can start.\n" +
            "Jump A must be completed before jump M can start.\n" +
            "Jump B must be completed before jump W can start.\n" +
            "Jump A must be completed before jump P can start.\n" +
            "Jump W must be completed before jump Q can start.\n" +
            "Jump R must be completed before jump X can start.\n" +
            "Jump M must be completed before jump H can start.\n" +
            "Jump F must be completed before jump S can start.\n" +
            "Jump K must be completed before jump Q can start.\n" +
            "Jump Y must be completed before jump Q can start.\n" +
            "Jump W must be completed before jump S can start.\n" +
            "Jump Q must be completed before jump T can start.\n" +
            "Jump K must be completed before jump H can start.\n" +
            "Jump K must be completed before jump D can start.\n" +
            "Jump E must be completed before jump T can start.\n" +
            "Jump Y must be completed before jump E can start.\n" +
            "Jump A must be completed before jump O can start.\n" +
            "Jump G must be completed before jump E can start.\n" +
            "Jump C must be completed before jump O can start.\n" +
            "Jump G must be completed before jump H can start.\n" +
            "Jump Y must be completed before jump I can start.\n" +
            "Jump V must be completed before jump S can start.\n" +
            "Jump B must be completed before jump R can start.\n" +
            "Jump B must be completed before jump X can start.\n" +
            "Jump V must be completed before jump I can start.\n" +
            "Jump N must be completed before jump J can start.\n" +
            "Jump H must be completed before jump I can start.";

    private Day18() {
    }

    public static void main(String[] args) {
        Day18 test = new Day18();
        test.test(DATA);
    }

    private void test(String data) {
        String[] lines = data.split("\n");
        Map<Character, Node> allNodes = new HashMap<>();

        for(String line: lines) {
            char beforeName = line.charAt(BEFORE_NODE_POS);
            Node beforeNode = allNodes.computeIfAbsent(beforeName, Node::new);
            char afterName = line.charAt(AFTER_NODE_POS);
            Node afterNode = allNodes.computeIfAbsent(afterName, Node::new);
            beforeNode.addChild(afterNode);
        }

        List<Node> topNodes = allNodes.values().stream().filter(node -> node.getParents().isEmpty()).collect(Collectors.toList());
//        System.out.println(topNodes);

        SortedSet<Node> availableNodes = new TreeSet<>(topNodes);
        while(!availableNodes.isEmpty()) {
            visitNode(availableNodes);
        }
        System.out.println();
    }

    private static void visitNode(SortedSet<Node> availableNodes) {
        Node node = availableNodes.first();
        node.complete();
        availableNodes.remove(node);
//        System.out.println(node);
        System.out.print(node.getName());
        for(Node child: node.getChildren()) {
            boolean allParentsCompleted = true;
            for(Node parent: child.getParents()) {
                if(!parent.isCompleted()) {
                    allParentsCompleted = false;
                    break;
                }
            }
            if(allParentsCompleted) {
                availableNodes.add(child);
            }
        }
    }
}

class Node implements Comparable<Node> {
    private final char name;
    private SortedSet<Node> parents = new TreeSet<>();
    private final SortedSet<Node> children = new TreeSet<>();
    private boolean completed;

    Node(char name) {
        this.name = name;
    }

    char getName() {
        return name;
    }

    void addChild(Node node) {
        children.add(node);
        node.parents.add(this);
    }

    SortedSet<Node> getChildren() {
        return children;
    }

    SortedSet<Node> getParents() {
        return parents;
    }

    boolean isCompleted() {
        return completed;
    }

    void complete() {
        completed = true;
    }

    @Override
    public int compareTo(Node o) {
        return Character.compare(name, o.name);
    }

    static void print(Node node) {
        System.out.println("Name: " + node.name);
        System.out.println("Children ---------------------------");
        node.children.forEach(Node::print);
    }

    @Override
    public String toString() {
        return "Node{" +
                "name=" + name +
                '}';
    }
}