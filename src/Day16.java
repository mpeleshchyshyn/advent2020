import java.time.LocalDate;

public class Day16 {
    private Day16() {
    }

    public static void main(String[] args) {
        Day16 test = new Day16();
        test.test();
    }

    private void test() {
        LocalDate currDate = LocalDate.of(2000, 1, 1);
        LocalDate endDate = currDate.plusYears(100);
        int count = 0;
        while(currDate.isBefore(endDate)) {
            int day = currDate.getDayOfMonth();
            int mounth = currDate.getMonthValue();
            int year = currDate.getYear() - 2000;
            if(mounth*mounth + day*day == year*year) {
                System.out.println(currDate);
                count++;
            }
            currDate = currDate.plusDays(1);
        }
        System.out.println("Count = " + count);
    }
}
